set -a
REPO_NAME=linux-stable-rc
KERNEL_BRANCH=linux-5.17.y
LATEST_SHA=59db887d13b3a4df2713c2a866fa2767e0dea569
KERNEL_REPO=https://gitlab.com/Linaro/lkft/mirrors/stable/linux-stable-rc
set +a
