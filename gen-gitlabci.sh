#!/bin/bash

ROOT_DIR="$(dirname "$(readlink -e "$0")")"

# Check arguments
if [ $# -eq 0 ]; then
  ( 
    echo "Error: One argument is required, the pipeline configuration."
    echo "  $0 .config"
  ) >&2
  exit 1
fi

config="$(readlink -e "$*")"
if [ ! -f "${config}" ]; then
  ( 
    echo "Error: File $* (${config}) could not be found."
  ) >&2
  exit 1
fi

check_env_variable() {
  if [ -z "${!1}" ]; then
    echo "Error: Variable ${1} not set in the environment" >&2
    exit 1
  fi
}

# Read configuration
set -a
# shellcheck disable=SC1090
source "${config}"
set +a

# Check environment
# The following variables are read from the environment, usually from
# a Gitlab CI job, then incorporated into the pipeline configuration:
#   CONFIG_KERNEL_BRANCH
#   CONFIG_LATEST_SHA
#   CONFIG_REPO_NAME
#   CONFIG_KERNEL_REPO
for var in CONFIG_KERNEL_BRANCH CONFIG_REPO_NAME CONFIG_KERNEL_REPO; do
  check_env_variable "${var}"
  env_var="${var/CONFIG_/}"
  export "${env_var}"="${!var}"
done

if [ -v CONFIG_LATEST_SHA ]; then
  LATEST_SHA="${CONFIG_LATEST_SHA}"
fi

# Try to resolve commit ID if only branch is given
if [ -z "${LATEST_SHA}" ] && [ -n "${KERNEL_BRANCH}" ]; then
  sha="$(git ls-remote "${KERNEL_REPO}" "refs/heads/${KERNEL_BRANCH}" | awk '{print $1}')"
  if [ -z "${sha}" ]; then
    echo "Could not resolve ${KERNEL_REPO} Git reference to a Git commit."
    exit 1
  fi
  LATEST_SHA="${sha}"
fi

# HACK: For linux-stable-rc
# shellcheck disable=SC2001
KERNEL_VERSION_MAJOR_MINOR="$(echo "${KERNEL_BRANCH#linux-*}" | sed -e 's:\.y$::')"
KERNEL_VERSION_MAJOR="$(echo "${KERNEL_VERSION_MAJOR_MINOR}" | cut -d. -f1)"
KERNEL_VERSION_MINOR="$(echo "${KERNEL_VERSION_MAJOR_MINOR}" | cut -d. -f2)"

# These stages are needed always
declare -a stages
stages=(download_prerequisites prerequisites build)

if [ "${CONFIG_TEST_SMOKE}" = "y" ]; then
  stages+=(sanity)
fi

# Write out stuff

## STAGES
echo "stages:"
for stage in "${stages[@]}"; do
  echo "  - ${stage}"
done
echo

## VARIABLES
cat << EOM
variables:
  REPO_NAME: $REPO_NAME
  KERNEL_BRANCH: $KERNEL_BRANCH
  LATEST_SHA: $LATEST_SHA
  KERNEL_REPO: $KERNEL_REPO
  GIT_STRATEGY: none
  tuxconfig: 'https://gitlab.com/Linaro/lkft/pipelines/lkft-common/-/raw/master/tuxconfig/master.yml'
  SKIPGEN_KERNEL_VERSION: "default"
  QA_SERVER: 'https://qa-reports.linaro.org'
  QA_TEAM: '~daniel.diaz'
  QA_PROJECT: $REPO_NAME-$KERNEL_BRANCH
  QA_PROJECT_NAME: "$REPO_NAME $KERNEL_BRANCH on OE"
  QA_PROJECT_SANITY: $REPO_NAME-$KERNEL_BRANCH-sanity
  QA_PROJECT_NAME_SANITY: "$REPO_NAME $KERNEL_BRANCH on OE - sanity"
  QA_WAIT_BEFORE_NOTIFICATION_TIMEOUT: 600
  QA_NOTIFICATION_TIMEOUT: 28800
  QA_FORCE_FINISHING_BUILDS_ON_TIMEOUT: "false"
  LAVA_SERVER: 'https://lkft.validation.linaro.org/RPC2/'
  default_lava_tests: "--test-plan lkft-full"
EOM
echo

## STAGE: download_prerequisites
### JOB: download_prerequisites
if [ -f "${ROOT_DIR}/stages/download_prerequisites-download_prerequisites.yml" ]; then
  cat "${ROOT_DIR}/stages/download_prerequisites-download_prerequisites.yml"
  echo
fi

## STAGE: prerequisites
### JOB: validateinputs
if [ -f "${ROOT_DIR}/stages/prerequisites-validateinputs.yml" ]; then
  cat "${ROOT_DIR}/stages/prerequisites-validateinputs.yml"
  echo
fi

## STAGE: prerequisites
### JOB: checkproject
if [ -f "${ROOT_DIR}/stages/prerequisites-validateinputs.yml" ]; then
  cat "${ROOT_DIR}/stages/prerequisites-checkproject.yml"
  echo
fi

## STAGE: build
### PROTOTYPE: .build
echo "####"
echo "#### BUILDS"
echo "####"
echo
if [ -f "${ROOT_DIR}/stages/build-.build.yml" ]; then
  cat "${ROOT_DIR}/stages/build-.build.yml"
  echo
fi

# $1: tuxconfig name
print_build_job() {
  tuxconfig_name="$1"
  job_name="build-${tuxconfig_name}"

  echo "${job_name}:"
  echo "  extends:"
  echo "    - .build"
  echo "  variables:"
  echo "    build_set_name: ${tuxconfig_name}"
  echo
}

# $1: buildset name
print_buildset_job() {
  buildset="$1"

  tuxconfig_name="${buildset,,}"

  job_name="buildset-${tuxconfig_name}"

  echo "${job_name}:"
  echo "  extends:"
  echo "    - .build"
  echo "  variables:"
  echo "    build_set_name: ${tuxconfig_name}"
  echo
}

if [ "${CONFIG_BUILD_ARCH_ARM_LKFT_GCC}" = "y" ]; then
  print_build_job arm-gcc
fi

if [ "${CONFIG_BUILD_ARCH_ARM64_LKFT_GCC}" = "y" ]; then
  print_build_job arm64-gcc
fi

if [ "${CONFIG_BUILD_ARCH_I386_LKFT_GCC}" = "y" ]; then
  print_build_job i386-gcc
fi

if [ "${CONFIG_BUILD_ARCH_X86_LKFT_GCC}" = "y" ]; then
  print_build_job x86-gcc
fi

if [ "${CONFIG_BUILD_ARCH_ARM_LKFT_CLANG}" = "y" ]; then
  print_build_job arm-clang-12
fi

if [ "${CONFIG_BUILD_ARCH_ARM64_LKFT_CLANG}" = "y" ]; then
  print_build_job arm64-clang-12
fi

if [ "${CONFIG_BUILD_ARCH_I386_LKFT_CLANG}" = "y" ]; then
  print_build_job i386-clang-12
fi

if [ "${CONFIG_BUILD_ARCH_X86_LKFT_CLANG}" = "y" ]; then
  print_build_job x86-clang-12
fi

if [ "${CONFIG_BUILD_ARCH_ARM_LKFT_DEBUG}" = "y" ]; then
  print_build_job arm-gcc-debug
fi

if [ "${CONFIG_BUILD_ARCH_ARM64_LKFT_DEBUG}" = "y" ]; then
  print_build_job arm64-gcc-debug
fi

if [ "${CONFIG_BUILD_ARCH_I386_LKFT_DEBUG}" = "y" ]; then
  print_build_job i386-gcc-debug
fi

if [ "${CONFIG_BUILD_ARCH_X86_LKFT_DEBUG}" = "y" ]; then
  print_build_job x86-gcc-debug
fi

if [ "${CONFIG_BUILD_ARCH_ARM_LKFT_KUNIT}" = "y" ]; then
  # Only 5.4+
  if [ "${KERNEL_VERSION_MAJOR}" -ge "5" ] && [ "${KERNEL_VERSION_MINOR}" -ge "4" ]; then
    print_build_job arm-gcc-kunit
  fi
fi

if [ "${CONFIG_BUILD_ARCH_ARM64_LKFT_KUNIT}" = "y" ]; then
  # Only 5.4+
  if [ "${KERNEL_VERSION_MAJOR}" -ge "5" ] && [ "${KERNEL_VERSION_MINOR}" -ge "4" ]; then
    print_build_job arm64-gcc-kunit
  fi
fi

if [ "${CONFIG_BUILD_ARCH_I386_LKFT_KUNIT}" = "y" ]; then
  # Only 5.4+
  if [ "${KERNEL_VERSION_MAJOR}" -ge "5" ] && [ "${KERNEL_VERSION_MINOR}" -ge "4" ]; then
    print_build_job i386-gcc-kunit
  fi
fi

if [ "${CONFIG_BUILD_ARCH_X86_LKFT_KUNIT}" = "y" ]; then
  # Only 5.4+
  if [ "${KERNEL_VERSION_MAJOR}" -ge "5" ] && [ "${KERNEL_VERSION_MINOR}" -ge "4" ]; then
    print_build_job x86-gcc-kunit
  fi
fi

if [ "${CONFIG_BUILD_ARCH_ARM_LKFT_RCUTORTURE}" = "y" ]; then
  print_build_job arm-gcc-rcutorture
fi

if [ "${CONFIG_BUILD_ARCH_ARM64_LKFT_RCUTORTURE}" = "y" ]; then
  print_build_job arm64-gcc-rcutorture
fi

if [ "${CONFIG_BUILD_ARCH_I386_LKFT_RCUTORTURE}" = "y" ]; then
  print_build_job i386-gcc-rcutorture
fi

if [ "${CONFIG_BUILD_ARCH_X86_LKFT_RCUTORTURE}" = "y" ]; then
  print_build_job x86-gcc-rcutorture
fi

if [ "${CONFIG_BUILD_ARCH_ARC_BUILDSET}" = "y" ]; then
  # Only 5.x
  if [ "${KERNEL_VERSION_MAJOR}" -ge "5" ]; then
    print_buildset_job arc
  fi
fi

if [ "${CONFIG_BUILD_ARCH_ARM_BUILDSET}" = "y" ]; then
  print_buildset_job arm
fi

if [ "${CONFIG_BUILD_ARCH_ARM64_BUILDSET}" = "y" ]; then
  print_buildset_job arm64
fi

if [ "${CONFIG_BUILD_ARCH_I386_BUILDSET}" = "y" ]; then
  print_buildset_job i386
fi

if [ "${CONFIG_BUILD_ARCH_MIPS_BUILDSET}" = "y" ]; then
  print_buildset_job mips
fi

if [ "${CONFIG_BUILD_ARCH_PARISC_BUILDSET}" = "y" ]; then
  # Only 5.x
  if [ "${KERNEL_VERSION_MAJOR}" -ge "5" ]; then
    print_buildset_job parisc
  fi
fi

if [ "${CONFIG_BUILD_ARCH_PPC_BUILDSET}" = "y" ]; then
  # Only 5.x
  if [ "${KERNEL_VERSION_MAJOR}" -ge "5" ]; then
    print_buildset_job ppc
  fi
fi

if [ "${CONFIG_BUILD_ARCH_RISCV_BUILDSET}" = "y" ]; then
  # Only 5.x
  if [ "${KERNEL_VERSION_MAJOR}" -ge "5" ]; then
    print_buildset_job riscv
  fi
fi

if [ "${CONFIG_BUILD_ARCH_S390_BUILDSET}" = "y" ]; then
  # Only 5.x
  if [ "${KERNEL_VERSION_MAJOR}" -ge "4" ] && [ "${KERNEL_VERSION_MINOR}" -ge "19" ]; then
    print_buildset_job s390
  fi
fi

if [ "${CONFIG_BUILD_ARCH_SH_BUILDSET}" = "y" ]; then
  # Only 5.x
  if [ "${KERNEL_VERSION_MAJOR}" -ge "5" ]; then
    print_buildset_job sh
  fi
fi

if [ "${CONFIG_BUILD_ARCH_SPARC_BUILDSET}" = "y" ]; then
  print_buildset_job sparc
fi

if [ "${CONFIG_BUILD_ARCH_X86_BUILDSET}" = "y" ]; then
  print_buildset_job x86
fi

# $1: Machine
# $2: Architecture
# $3: Build job dependency
print_sanity_job() {
  device_type="$1"
  arch="$2"
  build_job="$3"
  cat << EOM
sanity-${device_type}:
  extends:
    - .sanity
  variables:
    ARCH: ${arch}
    DEVICE_TYPE: ${device_type}
  needs:
    - ${build_job}
    - job: download_prerequisites
      artifacts: true
EOM
  echo
}

if [[ ${stages[*]} =~ sanity ]]; then
  ## STAGE: sanity
  ### PROTOTYPE: .submit-test
  echo "####"
  echo "#### SANITY"
  echo "####"
  echo
  if [ -f "${ROOT_DIR}/stages/sanity-.submit-test.yml" ]; then
    cat "${ROOT_DIR}/stages/sanity-.submit-test.yml"
    echo
    # Weird dependency for non-sanity tests.
    # TODO: Remove this line vvv when used by
    # the tests stage.
    # shellcheck disable=SC2034
    PROTO_SANITY_SUBMIT_TEST=1
  fi

  ## STAGE: sanity
  ### PROTOTYPE: .sanity
  if [ -f "${ROOT_DIR}/stages/sanity-.sanity.yml" ]; then
    cat "${ROOT_DIR}/stages/sanity-.sanity.yml"
    echo
  fi

  if [ "${CONFIG_TEST_SMOKE_MACHINE_I386}" = "y" ]; then
    print_sanity_job i386 i386 build-i386-gcc
  fi

  if [ "${CONFIG_TEST_SMOKE_MACHINE_DB845C}" = "y" ]; then
    print_sanity_job dragonboard-845c arm64 build-arm64-gcc
  fi

  if [ "${CONFIG_TEST_SMOKE_MACHINE_JUNO}" = "y" ]; then
    print_sanity_job juno-r2 arm64 build-arm64-gcc
  fi

  if [ "${CONFIG_TEST_SMOKE_MACHINE_X15}" = "y" ]; then
    print_sanity_job x15 arm build-arm-gcc
  fi

  if [ "${CONFIG_TEST_SMOKE_MACHINE_X86}" = "y" ]; then
    print_sanity_job x86 x86 build-x86-gcc
  fi
fi
